<nav class="finotab">
  <div class="nav_wrapper">
    <div class="wrapper-interno">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="titolo_sito_nav">
          <?php get_template_part('img/svg/logo_pcustom.svg'); ?>
    </a>
    <?php 
    /*  MENU MOBILE  */
    wp_nav_menu( array(
        'theme_location'  =>'menumobile' ,
        'container'       => '',
        'container_class' => false,
      )
    );


    custom_language_selector_mobile();
      ?>

    </div>
  </div>
</nav>
<div class="mobile-menu hamburger finotab" id="hamburger-menu" data-action="toggleMenu"><span class="line"></span><span class="line"></span><span class="line"></span></div>