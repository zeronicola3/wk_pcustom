<?php 
/* BLOCCO CASE HISOTRY TOP PROJECT*/
?>
	<div class="item-case case-overlay">
		<div class="case-slider case-slider-<?= $post->ID; ?>">
		    <ul class="slides">
		    	<?php 
		    	$gallery=get_post_meta($post->ID, "wpcf-galleria-realizzazione");
		    	$gallery=$gallery[0];
		    	
		    	$post_content = $gallery;
		    	preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
		    	$array_id = explode(",", $ids[1]);

		    	$random_gallery=rand(0,100000);

		    	foreach ($array_id as $image_id) {
		    		$img=wp_get_attachment_image_src( $image_id, 'medium' )[0];
		    		$img_big=wp_get_attachment_image_src( $image_id, 'large' )[0];
		    		?>
		    		<li>
		    			<a href="<?= $img_big ?>" data-gallery="#<?= $random_gallery; ?>" style="background-image:url('<?= $img ?>');">
		    			</a>
		    		</li>
		    		<?php
		    	}
		    	?>
		    </ul>
		</div>	
		<div class="item-title-wrap item-title-overlay">
			<h3><?php echo get_the_title($band->ID); ?></h3>
			<h5><?php echo get_post_meta($band->ID, "wpcf-luogo")[0]; ?></h5>
			<div class="wrap_pulsante wk_align_center"><a href="<?php echo get_the_permalink($band->ID); ?>" class="pulsante"><?php _e('discover this project', 'webkolm') ?></a></div>
		</div>
	</div>
<?php
?>
