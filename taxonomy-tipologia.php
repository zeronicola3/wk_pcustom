
<?php
/*
Template Name: TAXONOMY CASE HISTORY
*/
?>
<?php 
if($_GET['rel']!='tab'){
    
get_header();
// REDIRECT
?><div class="custom-wrapper"><?php
get_template_part('block_filtro-tipologia');
?><div id="singola-taxonomy"><?php


}

?><div class="wrap-taxonomy"><?php
      

      $tax = $wp_query->get_queried_object();
      echo '<div class="taxonomy-description">';

      echo '<h2>'.$tax->name.'</h2>';
      echo types_render_termmeta("informazioni", array( "term_id" => "$tax->term_id", ) );
      echo '</div>';

      /* I PRODOTTI DELLA CATEGORIA */
      global $wp_query;
      query_posts(
        array_merge(
          $wp_query->query,
          array(
          'orderby' => 'menu_order',
          'order'=>'ASC',
          'posts_per_page' => -1
          )
        )
      );

      
      while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
        $top_project=0;
        $top_project=get_post_meta($post->ID, "wpcf-realizzazione-pro");
        
        // SE È UN PROGETTO PRO MOSTRO IL CONTENT SVILUPPATO CON IL VISUAL COMPOSER
        if($top_project[0]=="1"){
          include('block_casehistory_top.php');
        }
        else{
        // ALTRIMENTI MOSTRO LA VISUALIZZAZIONE STANDARD
          include('block_casehistory.php');
        }

      endwhile;
      // reset post data (important!)
      wp_reset_postdata();
?></div><?php

if($_GET['rel']!='tab'){

?></div><?php
get_template_part('block_loader');
?></div><?php
get_footer();
// REDIRECT

}else{
  ?>
  <script>
    if($(".case-slider").length > 0){
      $('.case-slider').flexslider({
          animation: "fade",
          animationLoop: true,
          slideshow: true,
          slideshowSpeed : "4000",
           pauseOnHover: true,
          multipleKeyboard: true,
          keyboard: true,
          controlNav: true, 

      });
    }
  </script>
  <?php
}
?>
</div>

