<?php

 if($_GET['rel']!='tab'){
  get_header(); } else { ?>

<script>
     $(".overlay-close").click(function(){
          $("body").toggleClass("bloccoscroll");
          $("html").toggleClass("bloccoscroll");
          $("#singolo-overlay").removeClass("open");
          $(".wrapper-singolo").fadeOut();
          
          var paginaold="<?php echo $_POST['paginaold'];?>"
          window.history.pushState({path:paginaold},'',paginaold);
    });
</script>

<?php } ?>


<?php if(have_posts()) : while(have_posts()) : the_post(); 
    $thumb_l = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
    $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $url_m = $thumb_m['0'];
  ?>

  <div class="wrapper-singolo <?php if($_GET['rel']=='tab'){ echo "pagina-tab";}?>" >
    <span class="overlay-close"></span>
    <div class="singolo-materiale">
      <div class="wk_home_cover">
          <ul class="slides">
              <style>
                .slideimg-homecover { background-image:url(<?php echo $thumb_l['0']; ?>);}
                @media (min-width: 768px) {  .slideimg-homecover { background-image:url(<?php echo $url_m; ?>); } }
              </style>
              <li class="slideimg-homecover">
              </li>
          </ul>
      </div>
      <h1 class="title_home title_nowrap"><?php the_title(); ?></h1>
      <div class="testo_materiale">
        <?php the_content(); ?>
      </div>
    </div>

    <div class="related-project">
      <h2><?php _e('Related projects', 'webkolm');?></h2>
      <hr>


<?php endwhile; endif; ?>

    <?php /* ELENCO PROGETTI CON MATERIALE CORRELATO */ ?>
    <?php

          $child_posts = types_child_posts('collegamento');

          foreach ($child_posts as $child_post) {
              $band_id = wpcf_pr_post_get_belongs($child_post->ID, 'realizzazione');
           
              //You can also use WP Native API get_post_meta to get the parent post ID
              //as it is stored in a hidden custom field _wpcf_belongs_post-type-slug_id
              //$band_id = get_post_meta($child_post->ID, '_wpcf_belongs_band_id', true);
           
              $band = get_post($band_id);
              $post->ID=$band->ID;
              $top_project=0;
              $top_project=get_post_meta($band->ID, "wpcf-realizzazione-pro");
              
              // SE È UN PROGETTO PRO MOSTRO IL CONTENT SVILUPPATO CON IL VISUAL COMPOSER
              if($top_project[0]=="1"){
                include('block_casehistory_top_correlated.php');
              }
              else{
              // ALTRIMENTI MOSTRO LA VISUALIZZAZIONE STANDARD
                include('block_casehistory_correlated.php');
              }
          }
          echo ".";

          /*
          // PRIMI 10 progetti
          $args = array(
              'post_type' => 'realizzazione',
              'posts_per_page' => 5,
              'orderby' => 'menu_order',
              'order' => 'ASC'
          );
          $query = new WP_Query( $args );

          if ( $query->have_posts() ) :
                  while ( $query->have_posts() ) : $query->the_post(); 
                      
                      $top_project=0;
                      $top_project=get_post_meta($post->ID, "wpcf-realizzazione-pro");
                      
                      // SE È UN PROGETTO PRO MOSTRO IL CONTENT SVILUPPATO CON IL VISUAL COMPOSER
                      if($top_project[0]=="1"){
                        include('block_casehistory_top.php');
                      }
                      else{
                      // ALTRIMENTI MOSTRO LA VISUALIZZAZIONE STANDARD
                        include('block_casehistory.php');
                      }


                      

                   endwhile;
               wp_reset_postdata();
          endif;

          */
    ?>
  </div>
    
  </div>



<?php if($_GET['rel']!='tab'){ get_footer(); }else{


	?>
	<?php
} ?>
