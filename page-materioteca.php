<?php
/**
 * Template Name: Materioteca
 * 
 */
get_header();
?>
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    	<?php the_content(); ?>
    	<br/><br/><br/>
    	<?php 
    		// CICLO MATERIALI
    		$args = array(
    		    'post_type' => 'materiale',
    		    'posts_per_page' => -1,
    		);
    		$query = new WP_Query( $args );

    		if ( $query->have_posts() ) :
    		    while ( $query->have_posts() ) : $query->the_post(); 
    		    	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )[0];
    	?>
    	<div class="wk_box_materioteca reverse_mat">
            <div class="mat_text">
                <h3 class="mat_intro"><?= excerpt(20) ?></h3>
                <h1 class="open_overlay" href="<?= get_the_permalink();?>"><?php the_title();?></h1>
                <div class="wrap_pulsante wk_align_left"><span href="<?= get_the_permalink();?>" class="pulsante open_overlay"><?php _e('Discover this material', 'webkolm'); ?></span></div>
            </div>
            <div class="mat_slider">
                <ul class="slides">
                	<li>
	                    <div class="mat_image open_overlay" href="<?= get_the_permalink();?>">
	                        <div class="inner" style="background-image:url('<?= $thumb ?>');"></div>
	                    </div>
	                    <div  class="mat_frame open_overlay" href="<?= get_the_permalink();?>">
	                        <h4 class="mat_title">+<?php _e('related<br/>projects', 'webkolm'); ?></h4>
	                    </div>
	                </li>
    	        </ul>
            </div>
        </div>
        <?php 
                 endwhile;
             wp_reset_postdata();
        endif;
        ?>
        <br/><br/><br/>
    <?php endwhile; ?>
  <?php endif; ?>
<?php get_footer(); ?>