    <ul class="lista-categorie">
        <!--<li><a class="attivo"><?php _e("All categories", "webkolm");?></a></li>-->
        <?php
        $args = array( 'taxonomy' => 'tipologia' );
        $terms = get_terms('tipologia', $args);
        foreach ($terms as $term) {


            $term_link = get_term_link( $term );
            $current_page=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if($term_link==$current_page){$superattivo="superattivo";} else { $superattivo="";}
            echo '<li><a href="javascript:void(0);" data-url="'.esc_url($term_link).'" class="'.$superattivo.'">'.$term->name.'</a></li>';
        }    
        ?>
    </ul>
    <div class="wrap-filtro onlymobile">
        <!--<span class="filtro-categorie"><?php _e("Select a category", "webkolm");?></span>-->
        <div class="lista-filtro-categorie">
        <select id="lista-filtro-categorie">
            <option><?php _e("Select a category", "webkolm");?></option>
            <?php
                foreach ($terms as $term) {
                    $term_link = get_term_link( $term );
                    $current_page=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    if($term_link==$current_page){$superattivo="selected";} else { $superattivo="";}
                    echo '<option value="'.esc_url($term_link).'" '.$superattivo.'>';
                    echo $term->name;  
                    echo '</option>';
                }
            ?>
        </select>
        </div>
    </div>