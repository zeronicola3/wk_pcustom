<?php

/**
 * Funzione che fornisce una risposta ad una richiesta GET rest API 
 *  -   fornisce N post "realizzazione"
 *
 * @return $request - oggetto della richiesta rest API
 */
function pentagroup_rest_API( WP_REST_Request $request ) {

    global $post;
    // Prendo i parametri dalla richiesta
    $per_page = $request['per_page'];
    $lang = $request['lang'];
    $order = isset($request['order']) ? $request['order'] : '';
    $orderby = isset($request['orderby']) ? $request['orderby'] : 'date';

    $rests = [];

    // Prendo i risultati in italiano
    $items = get_posts( array(
        'post_type' => 'realizzazione',
        'posts_per_page' => $per_page,
        'suppress_filters' => 0,
        'order' => $order,
        'orderby' => $orderby
    ) );

    if ( !empty( $items ) ) {
        foreach ($items as $key => $item) {

            // prendo il post nella lingua specificata
            $item = get_post(icl_object_id($item->ID, 'realizzazione', FALSE, $lang));
            
            // prendo la prima immagine dalla galleria
            $gallery = get_post_meta($item->ID, "wpcf-galleria-realizzazione");
            $gallery = $gallery[0];
            
            $post_content = $gallery;
            preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
            $array_id = explode(",", $ids[1]);

            $url_small = wp_get_attachment_image_src( $array_id[0], 'medium' );


            $result['title'] = $item->post_title;
            $result['img'] = $url_small[0];
            $result['url'] = get_the_permalink( $item->ID);
            $result['location'] = get_post_meta($item->ID, "wpcf-luogo")[0];

            // aggiungo il risultato all'array
            array_push($rests, $result);
        }
    
    } else {
        return null;
    }

    return $rests;
}


add_action( 'rest_api_init', function () {
  register_rest_route( 'pentagroup/v1', '/realizzazione/', array(
    'methods' => 'GET',
    'callback' => 'pentagroup_rest_API',
  ) );
} );



?>