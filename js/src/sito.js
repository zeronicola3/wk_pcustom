/* DIMENSIONI SCHERMO */
	 
	var $width = $(window).width();
	var $height = $(window).height();
	var $docheight = document.documentElement.scrollHeight;
	var $scrollspace = $docheight-$height;

function is_touch_device() {
 return (('ontouchstart' in window)
      || (navigator.maxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
 //navigator.msMaxTouchPoints for microsoft IE backwards compatibility
}
	  
$(document).ready(function() {
    	
	// OTTIMIZZAZIONE PER RETINA */
	
	if (window.devicePixelRatio == 2) {

          var images = $("img.hires");

          // loop through the images and make them hi-res
          for(var i = 0; i < images.length; i++) {

            // create new image name
            var imageType = images[i].src.substr(-4);
            var imageName = images[i].src.substr(0, images[i].src.length - 4);
            imageName += "@x2" + imageType;

            //rename image
            images[i].src = imageName;
          }
     }
	 
	
	if(!is_touch_device()){
		
		$(".touch").addClass("notouch");
		$("body").addClass("notouch");
		
	}else{
		$("body").addClass("touch");
	}

	
	if($width<1000) {
		
		/* PER SMARTPHONE */

		/* TASTO PER APERTURA MENU */
		$("#hamburger-menu").on('click', function (){
		  $('body').toggleClass('nav-open');
		  $('html').toggleClass('nav-open');
		  $(this).toggleClass('is-active');
		});
		
		$("nav.onlymobile .menu").on('click', 'li a', function(){
			var $this=$(this);
			if($this.hasClass("attivo"))
			{
				$this.toggleClass("attivo");
				$this.next('ul.sub-menu').slideToggle();
			}
			else{
				$this.next('ul.sub-menu').slideToggle();
				$this.toggleClass("attivo");	
			}
			
			if($(this).siblings("ul.sub-menu").size()!=0){
				return false;
			}
		});
		
		
		$("nav.onlymobile .menu li.current-menu-ancestor > a").addClass("attivo");
	} 

	/* SLIDER SITO GENERICO */

	if($(".case-slider").length > 0){
		$('.case-slider').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    controlNav: true, 

		});
	}


	/* GESTIONE APERTURA MATERIALI SU SINGOLO OVERLAY */


	/* GESTION FILTRO CATEGORIE CUSTOM PROJECT */

	$(".lista-categorie li a").on('click', function(){
		 
		/* PASSO COME VARIABILE LA VECCHIA PAGE */
		var questo=$(this);
		//get the link location that was clicked
			$('#singola-taxonomy .wrap-taxonomy').fadeOut(500, function(){
				$('.loader_wrap').addClass("vuoto");
				$(".lista-categorie li a.superattivo").removeClass("superattivo");
				questo.addClass('superattivo');
				pageurl = questo.attr('data-url');
				//to get the ajax content and display in div with id 'content'
				$.ajax({url:pageurl+'?rel=tab',success: function(data){
					$('#singola-taxonomy').html(data);
					$(".loader_wrap").removeClass("vuoto");
					
				}});

				if(pageurl!=window.location){
  				window.history.pushState({path:pageurl},'',pageurl);
  				}

			});
			return false;

	});

	// bind filter on select change
	$('#lista-filtro-categorie').on( 'change', function() {
	  	pageurl = this.value;
  		//get the link location that was clicked
  			$('#singola-taxonomy .wrap-taxonomy').fadeOut(500, function(){
  				$('.loader_wrap').addClass("vuoto");
  				
  				//to get the ajax content and display in div with id 'content'
  				$.ajax({url:pageurl+'?rel=tab',success: function(data){
  					$('#singola-taxonomy').html(data);
  					$(".loader_wrap").removeClass("vuoto");
  					
  				}});

  				if(pageurl!=window.location){
  				window.history.pushState({path:pageurl},'',pageurl);
  				}



  			});
  			return false;
	});

	/* CAROUSEL POST

	$('.owl-carousel').owlCarousel({
	   loop:true,
	       margin:30,
	       responsiveClass:true,
	       autoplay:true,
	       dots:false,
	       nav:true,
	       navText:['&#60;','&#62;'],
	       responsive:{
	           0:{
	               items:1
	           },
	           600:{
	               items:2
	           },
	           1000:{
	               items:3
	           }
	           1280:{
	               items:4
	           }
	       }
	}); */


	// APRO OVERLAY
	$.ajaxSetup({cache:false});
        $(".open_overlay").click(function(event){
			
            $("body").toggleClass("bloccoscroll");
            $("html").toggleClass("bloccoscroll");

			
			$("#singolo-overlay").toggleClass("open");
			
			//get the link location that was clicked
			pageurl = $(this).attr('href');
            var post_link = $(this).attr("href");
			
			/* PASSO COME VARIABILE LA VECCHIA PAGE */
			var paginaold_very=window.location.href;
			
			
			//to get the ajax content and display in div with id 'content'
			$.ajax({
				type:"POST",
				url:pageurl+'?rel=tab',
				data: { paginaold : paginaold_very },
				success: function(data){
					$('#singolo-overlay').html(data);
					
					},
					cache: false
			});
			
			//to change the browser URL to the given link location
			if(pageurl!=window.location){
				 window.history.pushState({path:pageurl},'',pageurl);
				}
				
			
			
			
        return false;
    });


        // ABILITO SOLO MERCOLEDI NEL DATEPICKER DEGLI APPUNTAMENTI


	

});	/* FINE DOCUMENT READY */



(function($) {

  /**
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
*/

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

var win = $(window);

var allMods = $(".module");

allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.delay(500).addClass("come-in"); 
    } 
  });
  
});   

