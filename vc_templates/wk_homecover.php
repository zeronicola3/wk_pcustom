<?php 

/* HOME COVER */

add_action( 'vc_before_init', 'wk_homecover_build' );
function wk_homecover_build() {
    vc_map( array(
        "name" => __( "Home cover", "webkolm" ),
        "base" => "wk_homecover",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert cover slider for homepage", 'webkolm'),
        "class" => "wk_homecover",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Titolo",
                'param_name' => 'wk_mat_title',
                'value' => "",
                'description' => __( "Titolo grande", "webkolm" )
            ),
            //PARAMS GROUP
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'slides',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __( "Caption", "webkolm" ),
                        'param_name' => 'wk_slide_title',
                    ),
                    array(
                        "type" => "attach_image",
                        "holder" => "img",
                        "class" => "",
                        "heading" => __( "Select image", "webkolm" ),
                        "param_name" => "wk_slide_image",
                        "value" => "",
                    )
                )
            )
            
            
        )
    ) );
}


add_shortcode( 'wk_homecover', 'wk_homecover_func' );
function wk_homecover_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_mat_title' => '',
    ), $atts ) );

    $slides= vc_param_group_parse_atts( $atts['slides'] );
    $output='<div class="wk_home_cover">
            <ul class="slides">';

            // CICLO LE SLIDES
            $numslide=0;
            foreach( $slides as $slide ){
                $images_small = wp_get_attachment_image_src($slide['wk_slide_image'], 'medium')[0];
                $images_big = wp_get_attachment_image_src($slide['wk_slide_image'], 'large')[0];

                $output.='
                <style>
                  .slideimg-homecover'.'-'.$numslide.' { background-image:url('.$images_small.');}
                  @media (min-width: 768px) {  .slideimg-homecover'.'-'.$numslide.' { background-image:url('.$images_big.'); } }
                </style>
                <li class="slideimg-homecover'.'-'.$numslide.'">
                    <span class="caption">
                        '.$slide['wk_slide_title'].'
                    </span>
                </li>';
                $numslide++;
            }
   
                


    $output.='</ul>
        </div>
        <h1 class="title_home">'.$wk_mat_title.'</h1>';
    
    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $(".wk_home_cover").flexslider({
                animation: "fade",
                animationLoop: true,
                slideshow: true,
                slideshowSpeed : "2000",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
                controlNav: true,
              
            });
        </script>';


    return $output;
        
}

?>