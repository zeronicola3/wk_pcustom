<?php


add_action( 'vc_before_init', 'wk_videoframe_build' );
function wk_videoframe_build() {

    vc_map( array(
        "name" => __( "Video frame", "webkolm" ),
        "base" => "webkolm_videoframe",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Video frame", 'webkolm'),
        "class" => "wk-videoframe",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Video ID", "webkolm" ),
                'param_name' => 'wk_videoid',
                "description" => __("Insert just the ID of the video", 'webkolm'),
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Channel", "webkolm" ),
                "param_name" => "wk_channel",
                "value" => array( "youtube", "vimeo" ),
                "description" => __( "Choose between Youtube and Vimeo", "webkolm" )
            ),
        )
    ) );
}


add_shortcode( 'webkolm_videoframe', 'wk_videoframe_func' );
function wk_videoframe_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_videoid' => '',
        'wk_channel' => 'youtube',
    ), $atts ) );

    $player="";
    if($wk_channel=="youtube"){
        $player='<iframe src="https://www.youtube.com/embed/'.$wk_videoid.'?rel=0" frameborder="0" allowfullscreen></iframe>';
    }elseif($wk_channel=="vimeo"){
        $player='<iframe src="https://player.vimeo.com/video/'.$wk_videoid.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }

    // CREO SLIDER FLEXSLIDER
    $output.='<div class="videoframe">';

    $output.=$player;

    $output.='</div>';

    return $output;
}


?>