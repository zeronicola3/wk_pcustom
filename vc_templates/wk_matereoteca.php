<?php 

/* MATERIOTECA BOX */

add_action( 'vc_before_init', 'wk_matereoteca_build' );
function wk_matereoteca_build() {
    vc_map( array(
        "name" => __( "Matereoteca", "webkolm" ),
        "base" => "wk_matereoteca",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert matereoteca box", 'webkolm'),
        "class" => "wk_matereoteca",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Titolo",
                'param_name' => 'wk_mat_title',
                'value' => "",
                'description' => __( "Titolo grande", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Testo",
                'param_name' => 'wk_mat_text',
                'value' => "",
                'description' => __( "Testo di introduzione", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Pulsante",
                'param_name' => 'wk_mat_button',
                'value' => "",
                'description' => __( "Testo del pulsante", "webkolm" )
            )
            
        )
    ) );
}


add_shortcode( 'wk_matereoteca', 'wk_matereoteca_func' );
function wk_matereoteca_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_mat_text' => '',
        'wk_mat_title' => '',
        'wk_mat_button' => '',
    ), $atts ) );

    $link_matereoteca=get_permalink( icl_object_id(41, 'page', false) );

    $output='<div class="wk_box_materioteca spaziatura">
            <div class="mat_text">
                <h3 class="mat_intro">'.$wk_mat_text.'</h3>
                <h1>'.$wk_mat_title.'</h1>
                <div class="wrap_pulsante wk_align_left"><a href="'.$link_matereoteca.'" class="pulsante">'.$wk_mat_button.'</a></div>
            </div>
            <div class="mat_slider">
                <ul class="slides">';

    // SLIDER MATERIALI
    $args = array(
        'post_type' => 'materiale',
        'posts_per_page' => 5,
    );
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post(); 
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )[0];
                
                $output.='<li>
                    <a href="'.get_the_permalink().'" class="mat_image">
                        <div class="inner" style="background-image:url('.$thumb.');"></div>
                    </a>
                    <a href="'.get_the_permalink().'"  class="mat_frame">
                        <h4 class="mat_title">'.get_the_title().'</h4>
                    </a>
                </li>';
                

             endwhile;
         wp_reset_postdata();
    endif;


    $output.='</ul>
            </div>
        </div>';
    
    // JS SLIDER INIZIALIZZAZIONE
    global $javascript_append;
    $javascript_append.='
        <script>
            $(".mat_slider").flexslider({
                animation: "fade",
                animationLoop: true,
                slideshow: true,
                slideshowSpeed : "2000",
                 pauseOnHover: true,
                multipleKeyboard: true,
                keyboard: true,
                controlNav: true,
              
            });
        </script>';


    return $output;
        
}

?>