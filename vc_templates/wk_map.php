<?php


add_action( 'vc_before_init', 'wk_mappa_build' );
function wk_mappa_build() {

    vc_map( array(
        "name" => __( "Mappa", "webkolm-ita" ),
        "base" => "webkolm_mappa",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Mappa", 'webkolm-ita'),
        "class" => "wk-mappa",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "LAT coordinate", "webkolm-ita" ),
                'param_name' => 'wk_latitudine'
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "LONG coordinate", "webkolm-ita" ),
                'param_name' => 'wk_longitudine'
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __( "Stringa stile mappa", "webkolm-ita" ),
                'param_name' => 'wk_stilemappa'
            ),
            array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Marker", "webkolm" ),
                "param_name" => "wk_map_marker",
                "value" => "",
            )
        )
    ) );
}


add_shortcode( 'webkolm_mappa', 'wk_mappa_func' );
function wk_mappa_func( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'wk_latitudine' => '',
        'wk_longitudine' => '',
        'wk_stilemappa' => '',
        'wk_map_marker' => '',
    ), $atts ) );


    // CREO SLIDER FLEXSLIDER
    $output.='<div class="wk_mappa">';

    $output.=$wk_latitudine.$wk_longitudine.$wk_stilemappa.$wk_map_marker;

    $output.='</div>';

    return $output;
}


?>