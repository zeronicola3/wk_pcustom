<?php 

/* PULSANTE VK */

add_action( 'vc_before_init', 'wk_button_build' );
function wk_button_build() {
    vc_map( array(
        "name" => __( "Button", "webkolm" ),
        "base" => "wk_button",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert button", 'webkolm'),
        "class" => "wk_button",
        "category" => __( "webkolm addons", "webkolm"),
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Text",
                'param_name' => 'wk_text',
                'value' => "",
                'description' => __( "Button text", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Link",
                'param_name' => 'wk_link',
                'value' => "",
                'description' => __( "Button link", "webkolm" )
            ),
            array(
                "type" => "colorpicker",
                "heading" => __( "Color of the button", "webkolm" ),
                "param_name" => "primary_color",
                "value" => "",
                "description" => __( "Default is green (on hover white)", "webkolm" )
            ),
            array(
                "type" => "dropdown",
                "heading" => __( "Select alignment", "webkolm" ),
                "param_name" => "wk_alignment",
                "value" => array( "left", "center", "right" ),
                "description" => __( "Choose the alignment of the buttons (default left)", "webkolm" )
            )
            
        )
    ) );
}



add_shortcode( 'wk_button', 'wk_button_func' );
function wk_button_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_text' => '',
        'wk_link' => '',
        'wk_alignment' => '',
        'primary_color' => ''
    ), $atts ) );

    // COLOR
    if($primary_color==""){
        // STANDARD COLOR
        $primary_color="";
    }

    
    return '<div class="wrap_pulsante wk_align_'.$wk_alignment.'"><a href="'.$wk_link.'" class="pulsante" style="background:'.$primary_color.';  ">
                '.$wk_text.'
            </a></div>';
        
}

?>