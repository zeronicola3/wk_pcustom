<?php 

/* C2A BOX */

add_action( 'vc_before_init', 'wk_c2abox_build' );
function wk_c2abox_build() {
    vc_map( array(
        "name" => __( "Call to action Architetti", "webkolm" ),
        "base" => "wk_c2abox",
        "icon" => get_template_directory_uri() . "/img/VC/w.png",
        "description" => __("Insert i 4 box di call to action", 'webkolm'),
        "class" => "wk_c2abox",
        "category" => 'Webkolm Add-on',
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => "Titolo",
                'param_name' => 'wk_c2a_title',
                'value' => "in Architects<br/>we trust",
                'description' => __( "Titolo grande", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Pulsante Visita",
                'param_name' => 'wk_c2a_button',
                'value' => "plan a visit<br/>to our factory",
                'description' => __( "Testo del pulsante del primo box", "webkolm" )
            ),
            array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Immagine del primo box", "webkolm" ),
                "param_name" => "wk_c2a_image",
                "value" => "",
            ),
            array(
                'type' => 'textfield',
                'heading' => "Testo consulenza",
                'param_name' => 'wk_c2a_consulenza',
                'value' => "consulenza<br/>illuminotecnica",
                'description' => __( "Testo del box consulenza illuminotecnica", "webkolm" )
            ),
            array(
                'type' => 'textfield',
                'heading' => "Pulsante Visita",
                'param_name' => 'wk_c2a_button_ultimo',
                'value' => "send a photo<br/>start a project",
                'description' => __( "Testo del pulsante dell'ultimo box", "webkolm" )
            ),
            array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Immagine dell'ultimo box", "webkolm" ),
                "param_name" => "wk_c2a_image_ultimo",
                "value" => "",
            )
            
        )
    ) );
}


add_shortcode( 'wk_c2abox', 'wk_c2abox_func' );
function wk_c2abox_func( $atts ) {
    extract( shortcode_atts( array(
        'wk_c2a_title' => 'in Architects<br/>we trust',
        'wk_c2a_button' => 'plan a visit<br/>to our factory',
        'wk_c2a_image' => '',
        'wk_c2a_consulenza' => 'consulenza<br/>illuminotecnica',
        'wk_c2a_button_ultimo' => 'send a photo<br/>start a project',
        'wk_c2a_image_ultimo' => '',
    ), $atts ) );

    $link_architect=get_permalink( icl_object_id(37, 'page', false) );
    $link_visita=get_permalink( icl_object_id(81, 'page', false) );
    $link_consulenza=get_permalink( icl_object_id(83, 'page', false) );
    $link_progetto=get_permalink( icl_object_id(85, 'page', false) );


    if($wk_c2a_image==""){
        $wk_c2a_image="http://pentalight.it/wp-content/uploads/2015/10/penta-storia-1-1400x931.jpg";
    }else{
        $wk_c2a_image = wp_get_attachment_image_src($wk_c2a_image, 'medium')[0];
    }
    if($wk_c2a_image_ultimo==""){
        $wk_c2a_image_ultimo="http://pentalight.it/wp-content/uploads/2015/10/HOTEL-ROCCA-DEL-CASTELLANO-CAMOGLI-ITALY-700x525.jpg";
    }else{
        $wk_c2a_image_ultimo = wp_get_attachment_image_src($wk_c2a_image_ultimo, 'medium')[0];
    }

    function requireSvg(){
            ob_start();
            require('/srv/users/serverpilot/apps/pcustomlight/public/wp-content/themes/wk_pcustom/img/svg/lamp.svg.php');
            return ob_get_clean();
    }

    $svg=requireSvg();
    $output='<div class="wk_blocco_c2a">
            <hr class="module horizontal-1"></hr>
            <a href="'.$link_architect.'" class="item_c2a main-item">
                <h1>'.$wk_c2a_title.'</h1>
            </a>
            <hr class="onlymobile"></hr>
            <a href="'.$link_visita.'" class="item_c2a first-from-tab">
                <div class="inner" style="background-image:url('.$wk_c2a_image.');"></div>
                <div class="wrap_pulsante"><span class="pulsante">'.$wk_c2a_button.'</span></div>
            </a>
            <hr class="module horizontal-2"></hr>
            <a href="'.$link_consulenza.'" class="item_c2a consulenza">
                <h2>'.$wk_c2a_consulenza.'</h2>
                '.$svg.'
            </a>
            <hr class="onlymobile"></hr>
            <a href="'.$link_progetto.'" class="item_c2a">
                <div class="inner" style="background-image:url('.$wk_c2a_image_ultimo.');"></div>
                <div class="wrap_pulsante"><span class="pulsante">'.$wk_c2a_button_ultimo.'</span></div>
            </a>
            <hr class="module horizontal-3"></hr>
            <div class="vline vertical-1 module"></div>
            <div class="vline vertical-2 module"></div>
            <div class="vline vertical-3 module"></div>
            <div class="vline vertical-4 module"></div>
            <div class="vline vertical-6 module"></div>
        </div>';

  

    return $output;
        
}

?>