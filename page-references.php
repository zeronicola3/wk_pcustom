<?php
/**
 * Template Name: Custom references
 * 
 */
get_header();
?>
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    	<?php the_content(); ?>
        <div class="custom-wrapper">
        	
            <?php get_template_part('block_filtro-tipologia');?>


            <div id="singola-taxonomy">
            <div class="wrap-taxonomy">
                <?php 
                // PRIMI 10 progetti
                $args = array(
                    'post_type' => 'realizzazione',
                    'posts_per_page' => 10,
                    'orderby' => 'menu_order',
                    'order' => 'ASC'
                );
                $query = new WP_Query( $args );

                if ( $query->have_posts() ) :
                        while ( $query->have_posts() ) : $query->the_post(); 
                            
                            $top_project=0;
                            $top_project=get_post_meta($post->ID, "wpcf-realizzazione-pro");
                            
                            // SE È UN PROGETTO PRO MOSTRO IL CONTENT SVILUPPATO CON IL VISUAL COMPOSER
                            if($top_project[0]=="1"){
                              include('block_casehistory_top.php');
                            }
                            else{
                            // ALTRIMENTI MOSTRO LA VISUALIZZAZIONE STANDARD
                              include('block_casehistory.php');
                            }
                            

                         endwhile;
                     wp_reset_postdata();
                endif;
                ?>

            </div>
            </div>

            <?php get_template_part('block_loader');?>

        </div>
    <?php endwhile; ?>
  <?php endif; ?>
<?php get_footer(); ?>