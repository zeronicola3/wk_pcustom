        </div> <!-- FINE CONTENUTI -->


        <!-- BLUEIMPGALLERY -->
        <div id="singolo-overlay">
            <div id="loader-singolo"></div>
        </div>

        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev"></a>
        	<a class="next"></a>
        	<a class="close">x</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>

        <footer id="footer">
            <?php 

            if("auralis"=="auralis"){
                ?>
                <a href="https://auralis.lighting/" class="logo-penta" target="blank">
                    <img title="AURALIS" src="https://www.pcustomlight.it/wp-content/uploads/2021/10/logo-auralis-group-2.png" alt="AURALIS Group" width="150px" height="105px" /><br /><br /> PCUSTOM Light<br /><?php _e('is part of AURALIS','pcustom'); ?>
                </a>
                <?php
            }
            else{
                ?>
                <a href="http://www.pentalightgroup.it/" class="logo-penta" target="blank">
                    <!--<?php _e('P CUSTOM LIGHT is part of PENTA Group', 'webkolm');?><br/><br/>
                    <?php get_template_part('img/svg/logo_pentalight.svg'); ?>-->
                    <img title="PENTA Group" src="http://pentalight.it/wp-content/uploads/2018/04/logo_penta_group-1.png" alt="PENTA Group" width="100px" height="100px" /><br /><br /> PCUSTOM Light<br /><?php _e('is part of PENTA group','pcustom'); ?>
                </a>
                <?php
            }
            ?>
            
            <div class="footer-content">
                <?php
                
                $my_id = icl_object_id(22, 'page', false);
                $post_id = get_post($my_id);
                $content = $post_id->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]>', $content);
                echo $content;
                
                //echo do_shortcode('[templatera id="295"]');
                ?>
            </div>
        </footer> 

        <!-- CSS -->
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo bloginfo( 'stylesheet_directory' );?>/css/stile.css?v=3" />

        <!-- JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
        <!-- <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script> -->
        <script type="text/javascript" src="<?php echo bloginfo( 'stylesheet_directory' );?>/js/theme.js?v=3324234"></script>

        <?php //get_template_part( 'cookie' ); ?>
        <?php 
        global $javascript_append;
        echo $javascript_append; ?>
        <?php wp_footer();?> 
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107693247-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-107693247-1', { 'anonymize_ip': true });
        </script>


        <?php // DATEPICKER SOLO MERCOLEDI ?>
        <script>
        /*
            jQuery(function($){
                $('input[name="date-505"]').datepicker({
                    "dateFormat":"dd/mm/yyyy",
                    "firstDay":1,
                    "changeMonth":true,
                    "changeYear":true,
                    "controlType":"slider",
                    "addSliderAccess":true,
                    "sliderAccessArgs":{"touchonly":true},
                    "stepHour":1,
                    "stepMinute":1,
                    "stepSecond":1,
                    "noWeekends":true
                }).datepicker('option', $.datepicker.regional['it']).datepicker('option', 'beforeShowDay', $.datepicker.noWeekends).datepicker('option', 'minDate', "").datepicker('option', 'maxDate', "").datepicker('refresh');
            });
    */
jQuery(function($){
    var nuovaData=new Date();
    $('input[name="date-505"]').datepicker('option', "minDate", nuovaData).datepicker('option', "beforeShowDay", function(date){
        var day = date.getDay();
        return [(day != 1 && day != 2 && day != 4 && day != 5 && day != 6 && day != 0)];
    });
    /*
$('input[name="date-505"]').on('focus',function(){
    var nuovaData=new Date();
        $('input[name="date-505"]').datepicker('option', "minDate", nuovaData);
        $('input[name="date-505"]').datepicker('option', "beforeShowDay", function(date){
            var day = date.getDay();
            return [(day != 1 && day != 2 && day != 4 && day != 5 && day != 6 && day != 0)];
        });


});
*/

            

    });



        </script>


<!--- POPUP AURALIS ---->

<style>
    #auralis-popup {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 99999;
        background: rgba(0, 0, 0, 0.5); 
        backdrop-filter: blur(5px);
        overflow-y: scroll;
    }
    /*===============   BOX APERTURA */

    div#box_apertura {
      position: relative;
      left: 50%;
      background-color: #000;
      height: auto;
      z-index: 99999;
      -webkit-transform: translateX(-50%);
              transform: translateX(-50%);
      padding: 60px;
      max-width: 600px;
      width: 90%; }


      @media all and (min-height: 650px) {
         div#box_apertura  {
             margin-top: calc(50vh - 310px);
         }
      }

      a.alert_close {
          position: absolute;
          width: 20px;
          height: 20px;
          display: block;
          top: 30px;
          right: 30px;
          cursor: pointer;
          transition: transform .4s ease-in-out;
      }

      a.alert_close:hover {
          transform: rotate(90deg);
          transition: transform .4s ease-in-out;
      }

      a.alert_close img {
          display: block;
          width:  100%;
          height: 100%;
      }


      div#box_apertura .alert-container h2 {
        font-size: 1.1rem;
        text-align: center;
        margin-bottom: 40px; }
      div#box_apertura .alert-container {
        color: #844e99; }
        div#box_apertura .alert-container ul {
          list-style-type: none;
          padding-left: 50px; }
        div#box_apertura .alert-container ul li {
          font-weight: 400;
          padding: 5px 0;
          position: relative; }
          div#box_apertura .alert-container ul li::before {
            content: " ";
            width: 40px;
            height: 40px;
            background-image: url("http://altaviadolomitibellunesi.it/wp-content/themes/altavia_theme/img/flag_green.png");
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;
            display: block;
            position: absolute;
            left: -50px;
            top: 0;
            -webkit-transform: translateX(-50%);
                    transform: translateX(-50%); }

      div#box_apertura .alert-btn-container {
          margin: 50px 0 0 0;
          text-align: center;
      }
      div#box_apertura .pulsante-pieno {
        font-weight: 400;
        text-align: center;
        font-size: 14px;
        cursor: pointer; }

      a#accetta_box {
          color: white;
          text-decoration: none;
          text-transform: uppercase;
          font-family: 'Helvetica', sans-serif;
          padding: 10px;
          display: block;
          width: fit-content;
          margin: 0 auto 20px auto;
          border: 2px solid white;
          transition: all .4s ease-in-out;
      }

      a#accetta_box:hover {
          color: black;
          background-color: white;
          transition: all .4s ease-in-out;

      }

      div#box_apertura .pulsante-con-sottotitolo {
        flex-direction: column;
        margin-left: 40px; }
        div#box_apertura .pulsante-con-sottotitolo span.sottotitolo-btn {
          font-size: .8rem;
          text-transform: none;
          line-height: 1em; }

    .alert_cover {
        width: 100%;
    }

    .alert_cover a {
        height: 100%;
        width: fit-content;
        display: block;
        margin: 0 auto;
    }

    .alert_cover a img {
      width: 100%;
        width: auto;
        display: block;
        max-width: 300px;
    }

    .alert_cover_text {
        color: white;
        font-family: 'Helvetica', sans-serif;
        font-weight: 100;
        line-height: 1.3em;
        font-size: 15px;
        text-align: justify;
    }

    @media all and (max-width: 47.99em) {
      div#box_apertura {
        line-height: 1.3em;
        font-size: .5rem;
        padding: 20px; }
        div#box_apertura .alert-container h2 {
          font-size: 1.2rem;
          margin-bottom: 20px; }
        div#box_apertura .alert-container ul {
          padding-left: 30px; }
          div#box_apertura .alert-container ul li::before {
            width: 20px;
            height: 20px;
            left: -15px;
            top: 5px; }
        div#box_apertura .alert-btn-container {
          /*flex-direction: column;*/ }
          div#box_apertura .alert-btn-container .pulsante-pieno {
            font-size: .75rem;
            margin: 0;
            margin-bottom: 20px;
            padding: 10px; }
          div#box_apertura .alert-btn-container .pulsante-con-sottotitolo {
            margin: 0;
            margin-bottom: 20px; } 

            .alert_cover_text {
              font-size: 14px;
              margin: 0;
              padding:0;
            }

            .alert_cover {
                width: 60%;
                margin-left: 20%;
                margin-right: 20%;
            }

            .alert_cover a img {
              width: 150px;
            }

            div#box_apertura .alert-btn-container {
              margin-top: 30px;
            }

          }

</style>


<script>
    // COOKIE BOX APERTURA

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function setCookie(cname, cvalue) {
        var d = new Date();
        d.setTime(d.getTime() + (60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";";
    }

    function checkCookie(){
        var popup = getCookie("box_apertura");
        if (popup==null || popup=="")
        {
            var timeout = setTimeout(function(){
                $("#auralis-popup").fadeIn();
            }, 100);
        }
    }

    $(document).ready(function() {
      checkCookie();

      $("#accetta_box, .alert_close").on('click', function(){
          $("#auralis-popup").fadeOut();
          setCookie("box_apertura", 'accettato');
      });
    });
</script>
<?php 
// Da togliere, assieme ai caratteri $ nella variabile
//ICL_LANGUAGE_CODE = 'it';
?>

<!--
<div id="auralis-popup" style="display: none">
<div id="box_apertura">
    <a class="alert_close">
      <img src="https://auralis.lighting/wp-content/uploads/2021/10/cross.png" class="alert_close_img" />
    </a>
    <div class='alert-container'>
        <div class="alert_content">
          <div class="alert_cover">
              <a href="https://auralis.lighting/" target="_blank">
                <img src="https://auralis.lighting/wp-content/uploads/2021/10/AU21-LOGO-RGB-04.png">
              </a>
          </div>
          <p class="alert_cover_text">
              <?php 
              if(ICL_LANGUAGE_CODE == 'it'){ 

                ?>
                Il design decorativo e contemporaneo di Penta, l’illuminazione tecnica indoor e outdoor di Castaldi e i grandi Maestri del design di Arredoluce, si fondono in un esempio di eccellenza e creatività italiana, innovazione tecnologica e approccio progettuale.<br><br>
                Nasce Auralis, una realtà unica interamente dedicata alla luce.
              <?php } else { ?>
                The decorative yet contemporary design by Penta, the technical lighting indoor and outdoor by Castaldi, the Masters’ collections by Arredoluce, come together as an example of excellence and Italian creativity, technological innovation and design approach.<br><br>
                Auralis, a unique group of companies, entirely dedicated to the light.
              <?php } ?>
          </p>
        </div>
        <div class='alert-btn-container'>
            <a class="pulsante-pieno" id="accetta_box" href="https://auralis.lighting/" target="_blank">
            <?php 
            if(ICL_LANGUAGE_CODE == 'it'){ 

              ?>
              Scopri di più
            <?php } else { ?>
              Discover Auralis
            <?php } ?></a>
        </div>
    </div>
</div>
</div>
<!-- FINE POPUP AURALIS -->

    </body>
</html>